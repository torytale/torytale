=== Futura ===
Contributors: Will Carvalho
Requires at least: WordPress 4.4
Tested up to: WordPress 4.4
Version: 1.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: two-columns, right-sidebar, fluid-layout, responsive-layout, flexible-header, custom-menu, editor-style, featured-images, full-width-template, threaded-comments

Futura WordPress Theme, Copyright (C) 2016, Will Carvalho
Futura is distributed under the terms of the GNU GPL

== Description ==
Futura is a free WordPress theme designed by Will Carvalho in Sao Paulo, Brazil.
Feel free to use, modify and redistribute this theme as you like.
Futura is distributed under the terms of the GNU GPL v2.0 or later.
Enjoy!

== Usage ==
To customize the theme navigate to Appearance > Customize.

The landing page is a page template, it can be set to a fixed page in Settings -> Reading, set Front page displays to a static page.

To change certain content on the landing page use the previously created categories:
- Featured Header
- Featured Main
- Featured Parallex
- Featured Content

== Copyright ==

- Images screenshot.png, futura/img/logo.png, futura/img/logo.png, futura/admin/img/ are creations of Will Carvalho and licensed under GPL v2+

Futura WordPress Theme bundles the following third-party resources:

The HTML5 Shiv,
The HTML5 Shiv are licensed under the terms of the GNU GPL, Version 2 (or later) and MIT license
Source: https://github.com/aFarkas/html5shiv

Respond.js, Copyright 2011
Respond.js are licensed under the terms of the MIT license
Source: https://github.com/scottjehl/Respond

Josefin Slab font,
Genericons are licensed under the terms of the OFL SIL Open Font License, 1.1
Source: https://www.google.com/fonts/specimen/Josefin+Slab

wp-bootstrap-navwalker,
wp-bootstrap-navwalker are licensed under the terms of the GNU GPL, Version 2 (or later)
Source: https://github.com/twittem/wp-bootstrap-navwalker

WP Bootstrap Comment Walker,
WP Bootstrap Comment Walker are licensed under the terms of the GNU GPL, Version 2 (or later)
Source: https://github.com/ediamin/wp-bootstrap-comment-walker

Bootstrap, Copyright 2011-2015
Licensed under the MIT license
Source: http://getbootstrap.com
<!DOCTYPE html>
<html  <?php language_attributes(); ?>>
<head>
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="pingback" href="<?php esc_url( bloginfo( 'pingback_url' ) ); ?>" />
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <!----------------------
    ------------------------
    Navbar
    ------------------------
    ---------------------->
	<nav id="navbar" class="navbar navbar-default navbar-fixed-top <?php echo (is_admin_bar_showing() ? 'admin' : ''); ?>">
        <div class="container">
			<div class="row">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only"><?php _e( 'Toggle navigation', 'futura' ); ?></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
						<a class="navbar-brand" href="<?php echo esc_url( site_url() ); ?>"><?php bloginfo('name'); ?></a>
				</div>

				<?php
					wp_nav_menu( array(
						'menu'              => 'primary',
						'theme_location'    => 'primary',
						'depth'             => 2,
						'container'         => 'div',
						'container_class'   => 'collapse navbar-collapse col-md-11',
						'container_id'      => 'bs-example-navbar-collapse-1',
						'menu_class'        => 'nav navbar-nav',
						'fallback_cb'       => 'futura_navwalker::fallback',
						'walker'            => new futura_navwalker())
					);
				?>
			</div>
        </div>
    </nav>

    <!----------------------
    ------------------------
    Header
    ------------------------
    ---------------------->
	<header class="wh-100 <?php echo (is_page_template( 'landing_page.php' ) ? 'home' : 'page'); ?>">
		<!-- Slider -->
			<!-- pattern -->
			<span class="pattern"></span>
			<?php futura_slider(); ?>
        <!-- Logo -->
		<?php if ( has_header_image() ) { ?>
        <div class="page-header wh-100">
            <img class="logo"src="<?php esc_url( header_image() ); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" />
        </div>
		<?php } ?>
    </header>
<?php get_header(); ?>
    <div id="main" class="container-fluid solid">
        <div class="container">
            <div class="row">
				<div class="<?php echo (is_active_sidebar('sidebar') ? 'col-xs-12 col-md-8' : 'col-xs-12 col-md-12'); ?>">
                    <article>
                        <h2><?php the_title(); ?></h2>
                        <?php edit_post_link( __( 'Edit', 'futura' ) ); ?>
                        <?php if ( wp_attachment_is_image( $post->id ) ) : $att_image = wp_get_attachment_image_src( $post->id, "high"); ?>
                        <p class="attachment"><a href="<?php echo esc_url(wp_get_attachment_url($post->id)); ?>" title="<?php the_title(); ?>" rel="attachment"><img src="<?php echo $att_image[0];?>" width="<?php echo $att_image[1];?>" height="<?php echo $att_image[2];?>"  class="attachment-medium" alt="<?php $post->post_excerpt; ?>" /></a></p>
                        <?php else : ?>
                        <a href="<?php echo esc_url(wp_get_attachment_url($post->ID)) ?>" title="<?php echo esc_html( get_the_title($post->ID), 1 ) ?>" rel="attachment"><?php echo basename($post->guid) ?></a>
                        <?php endif; ?>
                        
                        <?php if ( !empty($post->post_excerpt) ) the_excerpt() ?>

						<blockquote>
						  <p><small><strong><?php the_author() ?></strong> <br /> <?php the_time('d/m/Y') ?></small></p>
						</blockquote>
						
                    </article>
				</div>
				<?php get_sidebar(); ?>
            </div>
        </div>
    </div>

<?php get_footer(); ?>

<?php get_header(); ?>
    <div id="main" class="container-fluid solid">
        <div class="container">
            <div class="row">
				<div class="<?php echo (is_active_sidebar('sidebar') ? 'col-xs-12 col-md-8' : 'col-xs-12 col-md-12'); ?>">
					<p><?php get_search_form(); ?></p>
					<h3><i><?php printf( __('Search Results for: %s', 'futura'), '<i>' . get_search_query() . '</i>' ); ?></i></h3>
                    <?php if ( have_posts() ) : ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <article>
                            <h2><a href="<?php echo esc_url( get_permalink($post->ID) ); ?>"><?php the_title(); ?></a></h2>
                            <?php esc_url( edit_post_link( __( 'Edit', 'futura' ) ) ); ?>
                            <?php the_excerpt(); ?>
                        </article>
                    <?php endwhile;
                        // Previous/next page navigation.
                        the_posts_pagination( array(
                            'mid_size' => 2,
                            'prev_text'          => '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>',
                            'next_text'          => '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>',
                        ) );
                        //No posts found.
                    else :
                    ?>
                        <article>
                            <p><?php esc_attr_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'futura'); ?> <span class="glyphicon glyphicon-hand-up" aria-hidden="true"></span></p>
                        </article>
                    <?php
                    endif;
                    ?>

				</div>
				<?php get_sidebar(); ?>
            </div>
        </div>
    </div>

<?php get_footer(); ?>

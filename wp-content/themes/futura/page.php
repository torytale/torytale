<?php get_header(); ?>
    <div id="main" class="container-fluid solid">
        <div class="container">
            <div class="row">
				<div class="<?php echo (is_active_sidebar('sidebar') ? 'col-xs-12 col-md-8' : 'col-xs-12 col-md-12'); ?>">
					<?php
					// Start the loop.
					while ( have_posts() ) : the_post(); ?>

						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<h2><?php the_title(); ?></h2>
							<?php esc_url( edit_post_link( __( 'Edit', 'futura' ) ) ); ?>
							<?php the_content(); ?>
							<h4><?php esc_url( wp_link_pages() );?></h4>
						</article>

					<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

					// End the loop.
					endwhile;
					?>
				</div>
				<?php get_sidebar(); ?>
            </div>
        </div>
    </div>
<?php get_footer(); ?>
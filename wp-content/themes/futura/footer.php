    <div class="container-fluid solid">
			<div class="container">
				<div class="row">
					<footer>
						<p class="pull-right text-center"><a href="#"><span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span><br /><?php _e( 'Back to top', 'futura' ); ?></a></p>
						<p>2016 <?php _e( 'Proudly powered by', 'futura' ); ?> <a target="_blank" href="http://williamcarvalho.net.br">Will Carvalho</a>
						<?php wp_footer(); ?>
					</footer>
				</div>
			</div>
    </div>
</body>
</html>
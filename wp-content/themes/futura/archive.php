<?php get_header(); ?>
    <div id="main" class="container-fluid solid">
        <div class="container">
            <div class="row">
				<div class="<?php echo (is_active_sidebar('sidebar') ? 'col-xs-12 col-md-8' : 'col-xs-12 col-md-12'); ?>">
				
					<?php $post = $posts[0];
					// Hack. Set $post so that the_date() works.
					/* If this is a category archive */
					if (is_category()) {
						printf('<h3>'.__('Category: %s', 'futura').'</h3>','<i>'.single_cat_title('', false).'</i>');
						//single_cat_title('', false)

					/* If this is a daily archive */
					} elseif (is_day()) {
						printf('<h3>'.__('Archive: %s', 'futura').'</h3>','<i>'.get_the_time('j \d\e F \d\e Y').'</i>');
						//get_the_time('j \d\e F \d\e Y')

					/* If this is a monthly archive */
					} elseif (is_month()) {
						printf('<h3>'.__('Archive: %s', 'futura').'</h3>','<i>'.get_the_time('F \d\e Y').'</i>');
						//get_the_time('F \d\e Y')

					/* If this is a yearly archive */
					} elseif (is_year()) {
						printf('<h3>'.__('Archive: %s', 'futura').'</h3>','<i>'.get_the_time('Y').'</i>');
						//get_the_time('Y')

					/* If this is an author archive */
					} elseif (is_author()) {
						printf('<h3>'.__('Archive: %s', 'futura').'</h3>','<i>'.get_the_author().'</i>');
						//get_the_author()

					/* If this is a paged archive */
					} elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {
						printf('<h3>'.__('Archive: %s', 'futura').'</h3>','<i>'.get_bloginfo('name').'</i>');
						//get_bloginfo('name')
					}
					?>
			<?php if ( have_posts() ) : ?>
			<?php
			while ( have_posts() ) : the_post();
			?>

                    <article>

                        <h2><a href="<?php echo esc_url(get_permalink($post->ID)); ?>"><?php the_title(); ?></a></h2>
                        <?php edit_post_link( __( 'Edit', 'futura' ) ); ?>
                        <?php the_excerpt(); ?>
                    </article>
            <?php
			endwhile;

			// Previous/next page navigation.
			the_posts_pagination( array(
				'mid_size'	=> 2,
				'prev_text'	=> '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>',
				'next_text'	=> '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>',
			) );

			//No posts found.
			else :
			?>
                <article>
					<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'futura' ); ?></p>
                    <?php get_search_form(); ?>
                </article>
            <?php
			endif;
			?>
              </div>
              <?php get_sidebar(); ?>
            </div>
        </div>
    </div>

<?php get_footer(); ?>

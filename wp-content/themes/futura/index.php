<?php get_header(); ?>
    <div id="main" class="container-fluid solid">
        <div class="container">
            <div class="row">
				<div class="<?php echo (is_active_sidebar('sidebar') ? 'col-xs-12 col-md-8' : 'col-xs-12 col-md-12'); ?>">
					<?php
					// Start the loop.
					while ( have_posts() ) : the_post(); ?>

						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
							<h2><a href="<?php echo esc_url( get_permalink($post->ID) ); ?>"><?php the_title(); ?></a></h2>
							<?php edit_post_link( __( 'Edit', 'futura' ) ); ?>
							<p><small><?php the_tags(); ?></small></p>
							<?php the_excerpt(); ?>
							<a class="btn btn-default" href="<?php esc_url( the_permalink() ); ?>" role="button"><?php _e( 'Read More', 'futura' ); ?></a>
						</article>

					<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
					// End the loop.
					endwhile;
						// Previous/next page navigation.
						the_posts_pagination( array(
							'mid_size' => 2,
							'prev_text'          => '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>',
							'next_text'          => '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>',
						) );
					?>
				</div>
				<?php get_sidebar(); ?>
            </div>
        </div>
    </div>
<?php get_footer(); ?>
	<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<div class="input-group input-group-lg">
		  <input type="text" class="form-control" placeholder="<?php _e( 'Search', 'futura' ); ?>" value="" name="s" aria-describedby="sizing-addon1">
			<span class="input-group-btn">
			<button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
			</span>
		</div>
	</form>

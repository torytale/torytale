<?php get_header(); ?>
    <div id="main" class="container-fluid solid">
        <div class="container">
            <div class="row">
				<div class="<?php echo is_active_sidebar('sidebar') ? 'col-xs-12 col-md-8' : 'col-xs-12 col-md-12'; ?>">
					<article>
						<h2><?php _e( 'Oops! That page can&rsquo;t be found.', 'futura' ); ?></h2>
						<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'futura' ); ?></p>
						<?php get_search_form(); ?>
					</article>
				</div>
				<?php get_sidebar(); ?>
            </div>
        </div>
    </div>

<?php get_footer(); ?>

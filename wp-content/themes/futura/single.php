<?php get_header(); ?>
    <div id="main" class="container-fluid solid">
        <div class="container">
            <div class="row">
				<div class="<?php echo (is_active_sidebar('sidebar') ? 'col-xs-12 col-md-8' : 'col-xs-12 col-md-12'); ?>">
					<?php
					// Start the loop.
					while ( have_posts() ) : the_post(); ?>

						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
							<h2><?php the_title(); ?></h2>
							<?php esc_url( edit_post_link( __( 'Edit', 'futura' ) ) ); ?>
							<p><small><?php the_tags(); ?></small></p>
							<?php the_content(); ?>
							<?php wp_link_pages( array('next_or_number'   => '', 'before' => '<nav><ul class="pager"><li>', 'after' => '</li></ul></nav>', ) ); ?>
							<?php if ( has_post_thumbnail() ) {
									$large_image_url = esc_url( wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' ) );
							?>
									<p><span class="label label-default">- <?php _e( 'Featured image', 'futura' ); ?> -</span></p>
									<a href="<?php echo $large_image_url[0]; ?>" title="<?php the_title_attribute(); ?>">
										<?php the_post_thumbnail(array(100, 100), array('class' => 'img-thumbnail')); ?>
									</a>
							<?php } ?>
						</article>


					<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
					// End the loop.
					endwhile;

					?>
				</div>
				<?php get_sidebar(); ?>
            </div>
        </div>
    </div>
<?php get_footer(); ?>
<?php
	/*****
	MENU OPTIONS
	*****/
	// Register Custom Navigation Walker
	require_once('scripts/wp_bootstrap_navwalker.php');

	function futura_custom_theme_setup() {

		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );
		$header = array(
			'flex-width'    => true,
			'width'         => 848,
			'flex-height'    => true,
			'height'        => 440,
			'default-image' => '',
		);
		add_theme_support( 'custom-header', $header );
		$bg = array(
			'default-color' => 'FFFFFF',
		);
		add_theme_support( 'custom-background', $bg );
	}
	add_action( 'after_setup_theme', 'futura_custom_theme_setup' );

	add_action('customize_register','futura_customize_register');
	function futura_customize_register( $wp_customize ) {
		$wp_customize->add_section(
		  'futura-options',
		  array(
			  'title' => __('Futura theme options', 'futura'),
			  'priority' => 10,
			)
		);
		
		$wp_customize->add_setting('parallax-background', array(
			'default'           => '',
			'sanitize_callback' => 'esc_url_raw'
		) );
		
		$wp_customize->add_control(
			new WP_Customize_Image_Control(
				$wp_customize,
				'parallax-background',
				array(
					'label' => __('Parallax Background', 'futura'),
					'section' => 'futura-options',
					'settings'  => 'parallax-background'
				)
			)
		);
	}
	
	load_theme_textdomain( 'futura', get_template_directory() . '/languages' );
	
	add_action("wp_enqueue_scripts", "futura_enqueue");
	
	function futura_enqueue() {
		wp_enqueue_style('googleapis', 'https://fonts.googleapis.com/css?family=Josefin+Slab:600', false, null);
		wp_enqueue_style('bootstrap', esc_url( get_template_directory_uri() ) . '/css/bootstrap.min.css', false, null);
		wp_enqueue_style( 'futura_style', esc_url( get_stylesheet_uri() ) );

		
		wp_enqueue_script('jquery');

		wp_enqueue_script( 'futura-html5', esc_url( get_template_directory_uri() ).'/js/html5shiv.js', false, null );
		wp_script_add_data( 'futura-html5', 'conditional', 'lt IE 9' );

		wp_enqueue_script( 'futura-respond', esc_url( get_template_directory_uri() ).'/js/respond.js', false, null );
		wp_script_add_data( 'futura-respond', 'conditional', 'lt IE 9' );

		wp_enqueue_script( 'futura-bootstrap', esc_url( get_template_directory_uri() ).'/js/bootstrap.min.js', false, null );
		
		wp_enqueue_script( 'slideshow', esc_url( get_template_directory_uri() ).'/js/slideshow.js', false, null );
	}




	add_action( 'init', 'futura_editor_styles' );
	function futura_editor_styles() {
	    add_editor_style( esc_url( get_stylesheet_uri() ) );
	}

	register_nav_menus( array(
	    'primary' => __('Primary Menu', 'futura'),
	) );

	function futura_theme_setup() {
	    add_theme_support( 'html5', array( 'comment-list' ) );
	}
	add_action( 'after_setup_theme', 'futura_theme_setup' );

	/**
	 * Register Sidebar
	 */
	function futura_register_sidebars() {

		/* Register the primary sidebar. */
		register_sidebar(
			array(
				'id'	=> 'sidebar',
				'name'	=> __('Sidebar', 'futura'),
				'description'	=> __('Right sidebar', 'futura'),
		        'before_widget' => '<div class="widget">',
		        'after_widget'  => '</div>',
		        'before_title'  => '<h3>',
		        'after_title'   => '</h3>',
			)
		);

		/* Repeat register_sidebar() code for additional sidebars. */
	}
	add_action( 'widgets_init', 'futura_register_sidebars' );

	add_filter('nav_menu_css_class' , 'futura_nav_class' , 10 , 2);
	function futura_nav_class($classes, $item){
	     if( in_array('current-menu-item', $classes) ){
	             $classes[] = 'active ';
	     }

	     if( in_array('menu-item-has-children', $classes) ){
	             $classes[] = 'dropdown ';
	     }
	     return $classes;
	}

	/*****
	CATEGORY
	*****/
	for ($i = 1; $i <= 4; $i++) {
		if($i==1){ $fname = 'Header'; }
		elseif($i==2){ $fname = 'Main'; }
		elseif($i==3){ $fname = 'Parallex'; }
		elseif($i==4){ $fname = 'Content'; }

		wp_insert_term('Featured '.$fname, 'category', array(
		'description'=> '',
		'slug'=>sanitize_title('featured-'.$fname),
		'parent'=>''
		));
	}

	/******
	Parallax
	******/
	function futura_parallax( $limit ) {
			$cont = 0; ?>
		<div class="bgParallax" style="background-image: url(<?php echo esc_url( get_theme_mod( 'parallax-background', '' ) ); ?>)">
			<div class="container text-center">
			<?php
            query_posts('category_name=Featured Parallex&showposts=3');

            if(have_posts()) : while(have_posts()) : the_post();
			
				if($limit=="first"){
				?>
					<div class="jumbotron">
						<h1><?php the_title(); ?></h1>
						<p><?php the_excerpt(); ?></p>
					</div>
				<?php
					break;
				} else {
					if($cont > 0){
						if ( has_post_thumbnail() ) {
							$url = esc_url( wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) ) );
							$link = esc_url( get_permalink(get_the_ID()) ); ?>
								<div class="col-xs-6 col-md-4">
									<a href="<?php echo $link; ?>" class="thumbnail">
										<img class="plus img-circle" src="<?php echo esc_url_raw( get_template_directory_uri() ); ?>/img/plus.png" >
										<?php the_post_thumbnail('', array( 'class'   => "img-circle")); ?>
									</a>
								</div>
						<?php
						}
					}
					$cont++;
				}
            endwhile; endif;
            wp_reset_query();
			?>
			</div>
			<span class="pattern"></span>
		</div>
	<?php
	}

	/******
	SLIDER
	******/
	function futura_slider() {
		query_posts('category_name=featured-header&showposts=4');
	    $a=0;
	    $url='';
	    if(have_posts()) : while(have_posts()) : the_post();

			if ( has_post_thumbnail() ) {
				$post = get_the_ID();
				$url = esc_url( wp_get_attachment_url( get_post_thumbnail_id($post) ) );
				$urls[$a] = $url;
				$a++;
			} else {
				$urls[0] = '';
			}

	    endwhile;
		endif;

		if(isset( $urls ) ){
			$count = count($urls);
		}else{
			$count = 0;
		}

	    if(!empty($count)){
	    	if($count > 1){

			    echo "<div id='slideshow' class='wh-100'>";
				for($i = 0; $i < $count; $i++){
					if($urls[$i]==''){
						$urls[$i] = "";
					}
				    echo "<div class='slider' style='background-image: url(".$urls[$i].")'></div>";
				}
				echo "</div>";
			} else {
				echo "<div class='no-slider' style='background-image: url(".$urls[0].")'></div>";	
			}
		} else {
			echo "<div class='no-slider' style='background-image: url(". esc_url( get_template_directory_uri() )."/img/slider.jpg)'></div>";
		}
		wp_reset_query();
	}


if ( ! isset( $content_width ) ) $content_width = 900;
if ( is_singular() ) wp_enqueue_script( "comment-reply" );
?>

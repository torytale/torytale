<?php
//Template Name: Landing Page
?>

<?php get_header(); ?>
    <!----------------------
    ------------------------
    Content
    ------------------------
    ---------------------->

    <div class="container-fluid solid">

        <div class="container">

            <!-- Sub header Options -->
            <div class="row text-center">
            <?php query_posts('category_name=featured-main&showposts=3');
            if(have_posts()) : while(have_posts()) : the_post();
            $link = esc_url( get_permalink($post->ID) ); ?>

              <div class="col-md-4">
                <h1><?php the_title(); ?></h1>
                <?php the_excerpt(); ?>
                <p><a class="btn btn-default" href="<?php echo $link; ?>" role="button"><?php _e( 'Read More', 'futura' ); ?> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></p>
              </div>
            <?php endwhile; ?>
            </div>
            <hr class="featurette-divider">
            <?php else : ?>
            </div>
            <?php
            endif;
            wp_reset_query(); ?>

            <!-- content -->
            <div class="row content">
            <?php query_posts('category_name=featured-content&showposts=1');
            if(have_posts()) : while(have_posts()) : the_post();
            ?>
                <div class="col-md-7 text-justify">
                    <?php the_content(); ?>
                </div>
                <div class="col-md-5">
                    <?php the_post_thumbnail(); ?>
                </div>
            <?php
            endwhile; endif;
            wp_reset_query(); ?>
            </div>
        </div>
    </div>

	<!-- Parallax -->
    <?php futura_parallax("first"); ?>


    <div class="container-fluid solid">
        <div class="container">
            <!-- TABLE -->
            <div class="row content">
            <?php query_posts('category_name=Featured Content&showposts=2');
            $cont = 1;
            if(have_posts()) : while(have_posts()) : the_post();
            if($cont==1){ } else { the_content(); }
            $cont ++;
            endwhile; endif;
            wp_reset_query(); ?>
            </div>
        </div>
    </div>

	<!-- Parallax -->
    <?php futura_parallax("Second"); ?>
	
    <div class="container-fluid solid">
        <div class="container">
            <div class="row content">
            <?php query_posts('category_name=Featured Content&showposts=3');
            $cont = 1;
            if(have_posts()) : while(have_posts()) : the_post();
            if($cont==1 || $cont==2){

            } else {
            ?>
                    <div class="col-md-5">
                        <?php the_post_thumbnail(); ?>
                    </div>
                    <div class="col-md-7 text-justify">
                        <?php the_content(); ?>
                    </div>
            <?php
            }
            $cont ++;
            endwhile; endif;
            wp_reset_query(); ?>
            </div>

            <hr class="featurette-divider">
        </div>
    </div>
<?php get_footer(); ?>
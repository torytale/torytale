<?php
// Do not delete these lines
    if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
        die();
 
    if ( post_password_required() ) { ?>
        <p class="nocomments"><?php _e( 'This article is restricted. Enter the password to view comments.', 'futura' ); ?></p>
    <?php
        return;
    }
?>
 <!-- Stack the columns on mobile by making one full-width and the other half-width -->
<div class="container-fluid comments">
	<div class="row">
	  <div class="col-xs-12 col-md-12">
		<blockquote><?php comments_number(__('No comments', 'futura' ), __('1 comment', 'futura' ), __('% comments', 'futura' ));?></blockquote>
	 
		<?php if ( have_comments() ) : ?>
	 
			<ul class="list-unstyled">
				<?php
					// Register Custom Comment Walker
					require_once('scripts/class-wp-bootstrap-comment-walker.php');

					wp_list_comments( array(
						'style'         => 'ul',
						'short_ping'    => true,
						'avatar_size'   => '64',
						'walker'        => new futura_Comment_Walker(),
					) );
				?>
			</ul><!-- .comment-list -->

			<?php paginate_comments_links('prev_text=<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>&next_text=<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>'); ?>

		<?php endif; ?>
	 
		<?php if ( comments_open() ) : ?>
	 
		<div id="respond">

				<h3><?php _e( 'Leave a comment!', 'futura' ); ?></h3>
	<?php
				$comments_args = array(
						// change the title of send button 
						'label_submit'=> __( 'Submit', 'futura' ),
						'class_submit'=> 'btn btn-default btn-lg btn-block',
						// change the title of the reply section
						'title_reply'=>'',
						// remove "Text or HTML to be displayed after the set of comment fields"
						'comment_notes_after' => '',
						'fields' => '<div class="form-group"><label class="sr-only" for="author">'.__( 'Name', 'futura' ).'</label><input type="text" name="author" id="author" required="required" value="'.$comment_author.'" class="form-control" placeholder="'.__( 'Name', 'futura' ).'"></div><div class="form-group"><label class="sr-only" for="email">'.__( 'Email', 'futura' ).'</label><input type="email" name="email" id="email" required="required" value="'.$comment_author_email.'" class="form-control" placeholder="'.__( 'Email', 'futura' ).'"></div>',
						// redefine your own textarea (the comment body)
						'comment_field' => '<div class="form-group"><label class="sr-only" for="author">'. __( 'Comment', 'futura' ) .'</label><textarea name="comment" id="comment" rows="10" cols="" class="form-control" placeholder="'.__( 'Comment', 'futura' ).'"></textarea></div>',
				);
				comment_form($comments_args);
	?>
			<p class="cancel"><?php cancel_comment_reply_link( __( 'Cancel comment.', 'futura' ) ); ?></p>
			</div>
		 <?php else : ?>
			<h3><?php _e( 'Comments are closed.', 'futura' ); ?></h3>
	<?php endif; ?>
		</div>
	</div>
</div>